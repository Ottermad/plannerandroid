package me.attwoodthomas.planner.sqlite;

import android.provider.BaseColumns;

/**
 * Created by charliethomas on 06/09/2015.
 */
public class HomeworkTableContact {
    public HomeworkTableContact() {}

    public static abstract class HomeworkTable implements BaseColumns {
        public static final String TABLE_NAME = "TBL_HOMEWORK";
        public static final String COLUMN_NAME_ID = "ID";
        public static final String COLUMN_NAME_SET_ID = "SET_ID";
        public static final String COLUMN_NAME_SET_NAME = "SET_NAME";
        public static final String COLUMN_NAME_DATE_DUE = "DATE_DUE";
        public static final String COLUMN_NAME_DESCRIPTION = "DESCRIPTION";
        public static final String COLUMN_NAME_TEACHER = "TEACHER";
    }
}
