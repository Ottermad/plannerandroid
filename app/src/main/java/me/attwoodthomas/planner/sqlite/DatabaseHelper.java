package me.attwoodthomas.planner.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by charliethomas on 06/09/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 14;
    public static final String DATABASE_NAME = "planner.db";
    private static final String TEXT = " TEXT";
    public static final String CREATE_PERIOD_TABLE =
            "CREATE TABLE " + PeriodTableContract.PeriodTable.TABLE_NAME + " (" +
                PeriodTableContract.PeriodTable.COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                PeriodTableContract.PeriodTable.COLUMN_NAME_WEEK + TEXT + "," +
                PeriodTableContract.PeriodTable.COLUMN_NAME_DAY + TEXT + "," +
                PeriodTableContract.PeriodTable.COLUMN_NAME_SET_NAME + TEXT + "," +
                PeriodTableContract.PeriodTable.COLUMN_NAME_PERIOD + TEXT + "," +
                PeriodTableContract.PeriodTable.COLUMN_NAME_SUBJECT + TEXT +
            ");";

    private static final String CREATE_HOMEWORK_TABLE =
            "CREATE TABLE " + HomeworkTableContact.HomeworkTable.TABLE_NAME + " (" +
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_ID + " INTEGER PRIMARY KEY, " +
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_ID + " INTEGER" + "," +
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_NAME + TEXT + "," +
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_DATE_DUE + TEXT + "," +
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_DESCRIPTION + TEXT + "," +
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_TEACHER + TEXT +
            ");";


    private static final String DELETE_PERIOD_TABLE =
            "DROP TABLE IF EXISTS " + PeriodTableContract.PeriodTable.TABLE_NAME;
    private static final String DELETE_HOMEWORK_TABLE =
            "DROP TABLE IF EXISTS " + HomeworkTableContact.HomeworkTable.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println(CREATE_HOMEWORK_TABLE);
        System.out.println(CREATE_PERIOD_TABLE);
        db.execSQL(CREATE_PERIOD_TABLE);
        db.execSQL(CREATE_HOMEWORK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        System.out.println(DELETE_HOMEWORK_TABLE);
        System.out.println(DELETE_PERIOD_TABLE);
        db.execSQL(DELETE_PERIOD_TABLE);
        db.execSQL(DELETE_HOMEWORK_TABLE);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
