package me.attwoodthomas.planner.sqlite;

import android.provider.BaseColumns;

/**
 * Created by charliethomas on 06/09/2015.
 */
public class PeriodTableContract {
    public PeriodTableContract() {}

    public static abstract class PeriodTable implements BaseColumns {
        public static final String TABLE_NAME = "TBL_PERIOD";
        public static final String COLUMN_NAME_ID = "ID";
        public static final String COLUMN_NAME_WEEK = "WEEK";
        public static final String COLUMN_NAME_DAY = "DAY";
        public static final String COLUMN_NAME_PERIOD = "PERIOD";
        public static final String COLUMN_NAME_SET_NAME = "SET_NAME";
        public static final String COLUMN_NAME_SUBJECT = "SUBJECT";
    }
}
