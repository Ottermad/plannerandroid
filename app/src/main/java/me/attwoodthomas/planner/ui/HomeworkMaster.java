package me.attwoodthomas.planner.ui;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.attwoodthomas.planner.R;
import me.attwoodthomas.planner.models.Homework;
import me.attwoodthomas.planner.models.Planner;
import me.attwoodthomas.planner.sqlite.DatabaseHelper;
import me.attwoodthomas.planner.sqlite.HomeworkTableContact;


public class HomeworkMaster extends ActionBarActivity {

    public ArrayList<Homework> mHomeworks;
    private DatabaseHelper mDatabaseHelper;
    @Bind(R.id.listView) ListView mListView;
    @Bind(R.id.progressBar) ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework_master);

        ButterKnife.bind(this);
        mDatabaseHelper  = new DatabaseHelper(this);
        mProgressBar.setVisibility(View.VISIBLE);
        new GetHomeworkInBackground().execute();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Homework homework = mHomeworks.get(position);
                Intent intent = new Intent(HomeworkMaster.this, HomeworkDetail.class);
                intent.putExtra("description", homework.getDescription());
                startActivity(intent);
            }
        });


    }

    private class GetHomeworkInBackground extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();

            String[] columnsToReturn = {
                    HomeworkTableContact.HomeworkTable.COLUMN_NAME_DESCRIPTION,
                    HomeworkTableContact.HomeworkTable.COLUMN_NAME_DATE_DUE,
                    HomeworkTableContact.HomeworkTable.COLUMN_NAME_ID,
                    HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_ID,
                    HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_NAME,
                    HomeworkTableContact.HomeworkTable.COLUMN_NAME_TEACHER
            };

            ArrayList<Homework> homeworks = new ArrayList<>();

            Cursor c = db.query(
                    HomeworkTableContact.HomeworkTable.TABLE_NAME,
                    columnsToReturn,
                    null,
                    null,
                    null,
                    null,
                    null);
            c.moveToFirst();

            while (!c.isAfterLast()) {

                // Get data from cursor
                String dateDue = c.getString(
                        c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_DATE_DUE)
                );

                String description = c.getString(
                        c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_DESCRIPTION)
                );

                int id = c.getInt(
                        c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_ID)
                );

                int setId = c.getInt(
                        c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_ID)
                );

                String setName = c.getString(
                        c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_NAME)
                );

                String teacher = c.getString(
                        c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_TEACHER)
                );

                System.out.println(homeworks);

                Homework homework = new Homework();
                homework.setDateDue(dateDue);
                homework.setDescription(description);
                homework.setId(id);
                homework.setId(setId);
                homework.setSetName(setName);
                homework.setTeacher(teacher);

                homeworks.add(homework);
                c.moveToNext();

            }

            mHomeworks = homeworks;
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            List<Map<String, Homework>> arrayList = new ArrayList<>();

            for (Homework homework: mHomeworks) {
                HashMap<String, Homework> homeworkHashMap = new HashMap<>();
                homeworkHashMap.put("homework", homework);
                arrayList.add(homeworkHashMap);
            }


            ArrayAdapter<Homework> adapter = new ArrayAdapter<Homework>(HomeworkMaster.this, android.R.layout.simple_list_item_1, mHomeworks);
            mListView.setAdapter(adapter);
            mProgressBar.setVisibility(View.INVISIBLE);

        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_homework_master, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

