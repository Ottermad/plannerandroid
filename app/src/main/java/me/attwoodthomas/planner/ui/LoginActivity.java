package me.attwoodthomas.planner.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.squareup.okhttp.Authenticator;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.Proxy;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.attwoodthomas.planner.R;
import me.attwoodthomas.planner.models.Planner;


public class LoginActivity extends ActionBarActivity {


    // Views
    @Bind(R.id.usernameEditText) EditText mUsernameEditText;
    @Bind(R.id.passwordEditText) EditText mPasswordEditText;


    // OnClicks
    @OnClick(R.id.loginButton) void submit() {
        String username = mUsernameEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();

        // Check if fields are empty
        if (username.isEmpty() || password.isEmpty()) {
            // Raise alert
        } else {
            // Make request
            checkAuthentication(username, password);
        }



    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }


    protected void checkAuthentication(final String username, final String password) {
        OkHttpClient client = new OkHttpClient();

        String url = Planner.mBaseUrl + "/token";

        client.setAuthenticator(new Authenticator() {
            @Override public Request authenticate(Proxy proxy, Response response) {
                System.out.println("Authenticating for response: " + response);
                System.out.println("Challenges: " + response.challenges());
                String credential = Credentials.basic(username, password);
                return response.request().newBuilder()
                        .header("Authorization", credential)
                        .build();
            }

            @Override public Request authenticateProxy(Proxy proxy, Response response) {
                return null; // Null indicates no attempt to authenticate.
            }
        });

        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = client.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e("Planner", e.getMessage());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (response.code() == 401) {
                    // TODO: Raise Alert
                    UserAuthErrorDialogFragment dialog = new UserAuthErrorDialogFragment();
                    dialog.show(getFragmentManager(), "error_dialog");
                } else if (response.isSuccessful()) {
                    // Set User Credentials
                    Context context = getApplicationContext();
                    SharedPreferences sharedPref = context.getSharedPreferences(
                            getString(R.string.preference_file_key), Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(getString(R.string.username_key), username);
                    editor.putString(getString(R.string.password_key), password);
                    editor.commit();

                    // Redirect
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);

                }
            }

        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
