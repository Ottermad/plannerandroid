package me.attwoodthomas.planner.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.okhttp.Authenticator;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;

import butterknife.ButterKnife;
import butterknife.OnClick;
import me.attwoodthomas.planner.R;
import me.attwoodthomas.planner.models.Homework;
import me.attwoodthomas.planner.models.Period;
import me.attwoodthomas.planner.models.Planner;
import me.attwoodthomas.planner.sqlite.DatabaseHelper;
import me.attwoodthomas.planner.sqlite.HomeworkTableContact;
import me.attwoodthomas.planner.sqlite.PeriodTableContract;


public class MainActivity extends ActionBarActivity {

    public static final String TAG = "MainActivity";
    public String mToken;
    protected DatabaseHelper mDatabaseHelper;

    @OnClick(R.id.button) void submit() {
        Intent intent = new Intent(MainActivity.this, HomeworkMaster.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mDatabaseHelper = new DatabaseHelper(this);

        System.out.println(mDatabaseHelper.getReadableDatabase().getPath());
        // Check User Credentials Exist
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        String username = sharedPref.getString("username", "");
        String password = sharedPref.getString("password", "");

        if (username.isEmpty() || password.isEmpty()) {
            // TODO: Login
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }

        // Get Token in background
        // Get homework & timetable
        getToken(username, password);
    }

    protected void getToken(final String username, final String password) {
        OkHttpClient client = new OkHttpClient();

        String url = Planner.mBaseUrl + "/token";

        client.setAuthenticator(new Authenticator() {
            @Override public Request authenticate(Proxy proxy, Response response) {
                System.out.println("Authenticating for response: " + response);
                System.out.println("Challenges: " + response.challenges());
                String credential = Credentials.basic(username, password);
                if (credential.equals(response.request().header("Authorization"))) {
                    return null; // If we already failed with these credentials, don't retry.
                }
                return response.request().newBuilder()
                        .header("Authorization", credential)
                        .build();
            }

            @Override public Request authenticateProxy(Proxy proxy, Response response) {
                return null; // Null indicates no attempt to authenticate.
            }
        });

        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        String resp = response.body().string();
                        JSONObject returnedData = new JSONObject(resp);
                        mToken = returnedData.getString("token");
                        getHomework();
                        getTimetable();
                    }
                } catch (IOException e) {
                    // TODO: Add Alert
                } catch (JSONException e) {
                    // TODO: Add Alert
                }
            }
        });
    }

    protected void getTimetable() {
        OkHttpClient client = new OkHttpClient();

        String url = Planner.mBaseUrl + "/timetable";

        client.setAuthenticator(new Authenticator() {
            @Override public Request authenticate(Proxy proxy, Response response) {
                System.out.println("Authenticating for response: " + response);
                System.out.println("Challenges: " + response.challenges());
                String credential = Credentials.basic(mToken, "");
                if (credential.equals(response.request().header("Authorization"))) {
                    return null; // If we already failed with these credentials, don't retry.
                }
                return response.request().newBuilder()
                        .header("Authorization", credential)
                        .build();
            }

            @Override public Request authenticateProxy(Proxy proxy, Response response) {
                return null; // Null indicates no attempt to authenticate.
            }
        });

        Request request = new Request.Builder()
                .url(url)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        ArrayList<Period> periods = parseTimetableResponse(responseString);
                        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
                        db.execSQL("DROP TABLE IF EXISTS " + PeriodTableContract.PeriodTable.TABLE_NAME);
                        db.execSQL(DatabaseHelper.CREATE_PERIOD_TABLE);
                        for (Period period: periods) {
                            long rowId = db.insert(
                                    PeriodTableContract.PeriodTable.TABLE_NAME,
                                    "null",
                                    period.getContentValues()
                            );

                        }
                        db.close();

                    }
                } catch (IOException e) {
                } catch (JSONException e) {
                }
            }
        });
    }

    protected void getHomework() {
        OkHttpClient client = new OkHttpClient();

        String url = Planner.mBaseUrl + "/homework";

        client.setAuthenticator(new Authenticator() {
            @Override
            public Request authenticate(Proxy proxy, Response response) {
                System.out.println("Authenticating for response: " + response);
                System.out.println("Challenges: " + response.challenges());
                String credential = Credentials.basic(mToken, "");
                if (credential.equals(response.request().header("Authorization"))) {
                    return null; // If we already failed with these credentials, don't retry.
                }
                return response.request().newBuilder()
                        .header("Authorization", credential)
                        .build();
            }

            @Override
            public Request authenticateProxy(Proxy proxy, Response response) {
                return null; // Null indicates no attempt to authenticate.
            }
        });
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        Homework[] homeworks = parseHomeworkResponse(responseString);
                        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
                        for (Homework homework: homeworks) {
                            long rowId = db.insert(
                                    HomeworkTableContact.HomeworkTable.TABLE_NAME,
                                    "null",
                                    homework.getContentValues()
                            );

                        }
                        db.close();
                    }
                } catch (IOException e) {
                } catch (JSONException e) {
                }
            }
        });
    }

    protected ArrayList<Period> parseTimetableResponse(String response) throws JSONException {
        ArrayList<Period> periods = new ArrayList<>();
        JSONObject base = new JSONObject(response);
        Iterator<String> keys = base.keys(); // A B
        while (keys.hasNext()) {
            String weekName = keys.next(); // A
            JSONObject week = base.getJSONObject(weekName);
            Iterator<String> weekKeys = week.keys(); // 1, 2
            while (weekKeys.hasNext()) {
                String dayName = weekKeys.next(); // 1
                JSONObject day = week.getJSONObject(dayName);
                Iterator<String> dayKeys = day.keys();
                while (dayKeys.hasNext()) {
                    String periodName = dayKeys.next();
                    JSONObject jsonPeriod = day.getJSONObject(periodName);
                    Period period = new Period();
                    period.setSetName(jsonPeriod.getString("set_name"));
                    period.setSubject(jsonPeriod.getString("subject"));
                    period.setDay(Period.mDays.get(Integer.parseInt(dayName)));
                    period.setPeriod(periodName);
                    period.setWeek(weekName);
                    periods.add(period);
                }
            }
        }

        return periods;
    }

    protected Homework[] parseHomeworkResponse(String response) throws JSONException{
        JSONObject base = new JSONObject(response);
        JSONArray jsonHomework = base.getJSONArray("homework");
        Homework[] homeworks = new Homework[jsonHomework.length()];
        for (int i = 0; i < jsonHomework.length(); i++) {
            JSONObject item = jsonHomework.getJSONObject(i);
            Homework homework = new Homework();
            homework.setDateDue(item.getString("date_due"));
            homework.setDescription(item.getString("description"));
            homework.setId(item.getInt("id"));
            homework.setSetId(item.getInt("set_id"));
            homework.setSetName(item.getString("set_name"));
            homework.setTeacher(item.getString("teacher"));
            homeworks[i] = homework;
        }
        return homeworks;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
