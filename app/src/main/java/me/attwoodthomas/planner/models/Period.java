package me.attwoodthomas.planner.models;

import android.content.ContentValues;

import java.util.HashMap;
import java.util.Map;

import me.attwoodthomas.planner.sqlite.PeriodTableContract;

/**
 * Created by charliethomas on 05/09/2015.
 */
public class Period {
    private String mWeek;
    private String mDay;
    private String mPeriod;
    private String mSetName;
    private String mSubject;

    public static final HashMap<Integer, String> mDays;

    static {
        HashMap<Integer, String> days = new HashMap<>();
        days.put(1, "Monday");
        days.put(2, "Tuesday");
        days.put(3, "Wednesday");
        days.put(4, "Thursday");
        days.put(5, "Friday");
        mDays = days;
    }

    public String getWeek() {
        return mWeek;
    }

    public void setWeek(String week) {
        mWeek = week;
    }

    public String getDay() {
        return mDay;
    }

    public void setDay(String day) {
        mDay = day;
    }

    public String getPeriod() {
        return mPeriod;
    }

    public void setPeriod(String period) {
        mPeriod = period;
    }

    public String getSetName() {
        return mSetName;
    }

    public void setSetName(String setName) {
        mSetName = setName;
    }

    public String getSubject() {
        return mSubject;
    }

    public void setSubject(String subject) {
        mSubject = subject;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(PeriodTableContract.PeriodTable.COLUMN_NAME_WEEK, mWeek);
        values.put(PeriodTableContract.PeriodTable.COLUMN_NAME_DAY, mDay);
        values.put(PeriodTableContract.PeriodTable.COLUMN_NAME_PERIOD, mPeriod);
        values.put(PeriodTableContract.PeriodTable.COLUMN_NAME_SET_NAME, mSetName);
        values.put(PeriodTableContract.PeriodTable.COLUMN_NAME_SUBJECT, mSubject);
        return values;
    }
}
