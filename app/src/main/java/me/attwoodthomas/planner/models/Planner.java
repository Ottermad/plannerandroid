package me.attwoodthomas.planner.models;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import me.attwoodthomas.planner.sqlite.DatabaseHelper;
import me.attwoodthomas.planner.sqlite.HomeworkTableContact;

/**
 * Created by charliethomas on 05/09/2015.
 */

public class Planner {

    // Member Variables
    public static String mBaseUrl = "https://planner-platform.herokuapp.com/api";

    public static ArrayList<Homework> getHomeworkDue(DatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String[] columnsToReturn = {
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_DESCRIPTION,
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_DATE_DUE,
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_ID,
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_ID,
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_NAME,
                HomeworkTableContact.HomeworkTable.COLUMN_NAME_TEACHER
        };

        ArrayList<Homework> homeworks = new ArrayList<>();

        Cursor c = db.query(
                HomeworkTableContact.HomeworkTable.TABLE_NAME,
                columnsToReturn,
                null,
                null,
                null,
                null,
                null);
        c.moveToFirst();

        while (!c.isAfterLast()) {

            // Get data from cursor
            String dateDue = c.getString(
                    c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_DATE_DUE)
            );

            String description = c.getString(
                    c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_DESCRIPTION)
            );

            int id = c.getInt(
                    c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_ID)
            );

            int setId = c.getInt(
                    c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_ID)
            );

            String setName = c.getString(
                    c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_NAME)
            );

            String teacher = c.getString(
                    c.getColumnIndex(HomeworkTableContact.HomeworkTable.COLUMN_NAME_TEACHER)
            );

            Homework homework = new Homework();
            homework.setDateDue(dateDue);
            homework.setDescription(description);
            homework.setId(id);
            homework.setId(setId);
            homework.setSetName(setName);
            homework.setTeacher(teacher);

            homeworks.add(homework);

        }

        return homeworks;
    }



}
