package me.attwoodthomas.planner.models;

import android.content.ContentValues;

import me.attwoodthomas.planner.sqlite.HomeworkTableContact;
import me.attwoodthomas.planner.sqlite.PeriodTableContract;

/**
 * Created by charliethomas on 04/09/2015.
 */
public class Homework {
    private String mDateDue;
    private String mDescription;
    private int mId;
    private int mSetId;
    private String mSetName;
    private String mTeacher;

    public String getDateDue() {
        return mDateDue;
    }

    public void setDateDue(String dateDue) {
        mDateDue = dateDue;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getSetId() {
        return mSetId;
    }

    public void setSetId(int setId) {
        mSetId = setId;
    }

    public String getSetName() {
        return mSetName;
    }

    public void setSetName(String setName) {
        mSetName = setName;
    }

    public String getTeacher() {
        return mTeacher;
    }

    public void setTeacher(String teacher) {
        mTeacher = teacher;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(HomeworkTableContact.HomeworkTable.COLUMN_NAME_ID, mId);
        values.put(HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_ID, mSetId);
        values.put(HomeworkTableContact.HomeworkTable.COLUMN_NAME_SET_NAME, mSetName);
        values.put(HomeworkTableContact.HomeworkTable.COLUMN_NAME_DATE_DUE, mDateDue);
        values.put(HomeworkTableContact.HomeworkTable.COLUMN_NAME_DESCRIPTION, mDescription);
        values.put(HomeworkTableContact.HomeworkTable.COLUMN_NAME_TEACHER, mTeacher);
        return values;
    }

    @Override
    public String toString() {
        return getSetName() + " - " + getTeacher();
    }

}
